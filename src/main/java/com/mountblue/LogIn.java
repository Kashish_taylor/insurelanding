package com.mountblue;

import org.hibernate.Session;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;


@WebServlet(value = "/LogIn")
public class LogIn  extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        EntityManagerFactory entityManagerFactory= Persistence.
                createEntityManagerFactory("insurelanding");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        Admin admin=entityManager.find(Admin.class, req.getParameter("email"));
        if(admin != null && admin.getPassword().equals(req.getParameter("password"))) {
            entityManager.getTransaction().begin();
            Query query =entityManager.createQuery("from ContactUs");
            List<ContactUs> contactUsList=query.getResultList();
            entityManager.getTransaction().commit();
                HttpSession session=req.getSession();
                session.setAttribute("name",admin.getName());
                req.setAttribute("contactUsList",contactUsList);
                req.getRequestDispatcher("showData.jsp").forward(req, resp);
        }
        else {
            resp.sendRedirect("login.jsp");
        }
    }
}
