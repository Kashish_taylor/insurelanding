package com.mountblue;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "/SignUp")
public class SignUp extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       Admin admin =new Admin();
       if(req.getParameter("password").equals(req.getParameter("confirmPassword"))) {
           admin.setName(req.getParameter("name"));
           admin.setEmail(req.getParameter("email"));
           admin.setPassword(req.getParameter("password"));
       }
       else {
           resp.sendRedirect("signUp.jsp");
       }
        EntityManagerFactory entityManagerFactory= Persistence.createEntityManagerFactory("insurelanding");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        if(entityManager.find(Admin.class, admin.getEmail())==null)
        {
            entityManager.getTransaction().begin();
            entityManager.persist(admin);
            entityManager.getTransaction().commit();
            resp.sendRedirect("login.jsp");
        }
        else
        {
            resp.sendRedirect("login.jsp");
        }
    }
}
