package com.mountblue;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "/ContactUsSubmit")
public class ContactUsSubmit extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ContactUs contactUs=new ContactUs();
        contactUs.setName(req.getParameter("name"));
        contactUs.setPhoneNo(Long.parseLong(req.getParameter("phoneno")));
        contactUs.setEmail(req.getParameter("email"));
        contactUs.setQuery(req.getParameter("query"));

        EntityManagerFactory entityManagerFactory= Persistence.createEntityManagerFactory("insurelanding");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(contactUs);
        entityManager.getTransaction().commit();
        resp.sendRedirect("index.jsp");
    }
}
