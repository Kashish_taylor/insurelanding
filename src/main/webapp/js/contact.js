function validate()
{
    const name=document.getElementById("name").value;
    const password=document.getElementById("phoneNo").value;
    const emailID=document.getElementById("emailID").value;
    const query=document.getElementById("query").value;
    if(name.trim() != "" || !/^[^a-zA-Z ]+$/.test(name) ||name.length<8) {
        document.getElementById("wrongName").style.visibility="hidden";
        document.getElementById("name").style.border="none";
    }
    if(password.trim() != "" || password.length==10 || /^[6-9][0-9]{9}$/.test(password)) {
        document.getElementById("wrongPhoneNo").style.visibility="hidden";
         document.getElementById("phoneNo").style.border="none";
    }
    if(/^.*@.*\.com$/.test(emailID))
    {
        document.getElementById("wrongEmailID").style.visibility="hidden";
         document.getElementById("emailID").style.border="none";
    }
    if(query != ""){
        document.getElementById("wrongquery").style.visibility="hidden";
        document.getElementById("query").style.border="none";
    }
    if(name.trim() == "" ) {
       document.getElementById("wrongName").style.visibility="visible";
       document.getElementById("wrongName").innerHTML="name can't be empty";
        document.getElementById("name").style.border="solid red";
        return false;
    }
    else if(/^[^a-zA-Z ]+$/.test(name))
    {
        document.getElementById("wrongName").style.visibility="visible";
        document.getElementById("wrongName").innerHTML="must be character";
         document.getElementById("name").style.border="solid red";
         return false;
    }
    else if(name.length<8) {
        document.getElementById("wrongName").style.visibility="visible";
        document.getElementById("wrongName").innerHTML="must be 8 character";
         document.getElementById("name").style.border="solid red";
         return false;
    }
    else if(password.trim() == "") {
        document.getElementById("wrongPhoneNo").style.visibility="visible";
        document.getElementById("wrongPhoneNo").innerHTML="Phone No should not be empty";
         document.getElementById("phoneNo").style.border="solid red";
        return false;
    }
    else if(password.length!=10) {
        document.getElementById("wrongPhoneNo").style.visibility="visible";
        document.getElementById("wrongPhoneNo").innerHTML="phone should be of 10diit";
         document.getElementById("phoneNo").style.border="solid red";
        return false;
    }
    else if(! /^[6-9][0-9]{9}$/.test(password))
    {   
        document.getElementById("wrongPhoneNo").style.visibility="visible";
        document.getElementById("wrongPhoneNo").innerHTML="phone should start with 6,7,8,9 and can have only digit";
        document.getElementById("phoneNo").style.border="solid red";
        return false;

    }
    else if(! /^.*@.*\.com$/.test(emailID)){
        document.getElementById("wrongEmailID").style.visibility="visible";
        document.getElementById("wrongEmailID").innerHTML="should be like @example.com";
        document.getElementById("emailID").style.border="solid red";
        return false;
    }
    else if(query == ""){
        document.getElementById("wrongquery").style.visibility="visible";
        document.getElementById("wrongquery").innerHTML="should not be empty";
        document.getElementById("query").style.border="solid red";
        return false;
    }
    else{
        return true;
    }
}

