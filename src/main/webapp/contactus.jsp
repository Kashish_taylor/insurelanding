<html>
    <head>
    <link rel="stylesheet" type="text/css" href="./css/contactus.css">
   <script src="./js/contact.js"></script>
    </head>
    <body>
    <%@include file="header.jsp"%>

    <h1>Contact US</h1>
    <form onsubmit="return validate()" action="ContactUsSubmit" method="post">
        <div>
            <p>User Name:</p>
            
            <input type="text" placeholder="Enter Name" id="name"  onfocus="this.value=''" name="name">
            <label id="wrongName"></label>
        </div>
        <div>
            <p>Phone No</p>`
            
            <input type="text" placeholder="Enter PhoneNo" id="phoneNo"  onfocus="this.value=''" name="phoneno">
            <label id="wrongPhoneNo"></label>
        </div>
        <div>
            <p>Email-ID</p>
            
            <input type="text" placeholder="Enter Email-ID" id="emailID"  onfocus="this.value=''" name="email">
            <label id="wrongEmailID"></label>
        </div>
        <div>
            <p>Query</p>
            <textarea placeholder="enter query" id="query" name="query"></textarea>
            <label id="wrongquery"></label>
        </div>
        <button type="submit" id="submit">Submit</button>
        
    </form>
    <%@include file="footer.jsp"%>
    </body>
</html>