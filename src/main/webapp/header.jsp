       <html>
        <head>
        <link rel="stylesheet" type="text/css" href="./css/header.css">
    <link rel="stylesheet" type="text/css" href="./css/hide.css">

        </head>
        <body>
        <div id="container">
            <header>
                <div>
                    <a href="index.jsp"><img src="./images/logo.svg" class="logo"></a>
                </div>
                <nav>
                    <ul>
                        <li><a href="#how_we_work" class="restTitle">HOW WE WORK</a></li>
                        <li><a href="login.jsp" class="restTitle">LOGIN</a></li>
                        <li><a href="signup.jsp" class="restTitle">SIGNUP</a></li>
                        <li><a href="#view_plans" class="viewPlans">VIEW PLANS</a></li>
                    </ul>
                </nav>
                <div id="hamburger" onclick="hunger()"><a href="#"><img src="/images/icon-hamburger.svg"></a></div>
                <div id="iconClose" onclick="closeIcon()"><a href="#"><img src="/images/icon-close.svg"></a></div>
            </header> 
        </div>
        </body>
        </html>