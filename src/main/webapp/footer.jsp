<html>
    <head>
    <link rel="stylesheet" type="text/css" href="./css/footer.css">
    <link rel="stylesheet" type="text/css" href="./css/hide.css">
    </head>
    <body>
        <div class="footer">
            <img src="./images/bg-pattern-footer-desktop.svg" class="img">
            <img src="./images/bg-pattern-footer-mobile.svg" class="footerimg">
            <footer id="footer">
                <section class="mainFooter">
                    <div><img src="./images/logo.svg"></div>
                    <ul>
                        <li class="right"><a href="#facebook"><img src="./images/icon-facebook.svg"></a></li>
                        <li class="right"><a href="#twitter"><img src="./images/icon-twitter.svg"></a> </li>
                        <li class="right"><a href="#pinterest" ><img src="./images/icon-pinterest.svg"></a></li>
                        <li class="right"><a href="#instagram" ><img src="./images/icon-instagram.svg"></a></li>
                    </ul>
                </section>
                <hr>
                <div class="row">
                    <section>
                            <ul>
                                <li class="head1"><a href="#" id="head-line">OUR COMPANY</a></li>
                                <li><a href="#" >HOW WE WORK</a></li>
                                <li><a href="#">WHY INSURE?</a></li>
                                <li><a href="#">VIEW PLANS</a></li>
                                <li><a href="#">REVIEWS</a></li>
                            </ul>
                    </section>
                    <section>
                            <ul >
                                <li class="head1"><a href="#" id="head-line">HELP ME</a></li>
                                <li><a href="#">FAQ</a></li>
                                <li><a href="#">TERMS OF SUE</a></li>
                                <li><a href="#">PRIVACY POLICY</a></li>
                                <li><a href="#">COOKIES</a></li>
                            </ul>
                    </section>
                    <section >
                            <ul>
                                <li class="head1"><a href="#" id="head-line">CONTACT</a></li>
                                <li><a href="#">SALES</a></li>
                                <li><a href="contactus.jsp">SUPPORT</a></li>
                                <li><a href="#">LIVE CHAT</a></li>
                                <li></li>
                            </ul>
                    </section>
                    <section>
                            <ul>
                                <li class="head1"><a href="#" id="head-line">OTHERS</a></li>
                                <li><a href="#">CAREERS</a></li>
                                <li><a href="#">PRESS</a></li>
                                <li><a href="#">LICENCES</a></li>
                                <li></li>
                            </ul>
                    </section>
                </div>
                
            </footer>
        </div>
    </body>
</html>