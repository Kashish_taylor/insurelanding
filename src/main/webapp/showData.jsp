<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="com.mountblue.ContactUs,java.lang.String,java.util.List"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Insure</title>
    <link rel="icon" href="images/favicon-32x32.png" type="image/png" sizes="32*32">
    <link rel="stylesheet" href="css/showdata.css">
    <style>
        @import url('https://fonts.googleapis.com/css?family=Karla&display=swap');
        @import url('https://fonts.googleapis.com/css?family=DM+Serif+Display&display=swap');
    </style>
</head>
<body>
<header class="header-class">
    <div class="mobile-header">
        <nav class="insure-icon">
            <a href="show-data.jsp" class="insure-icon"><img src="images/logo.svg"></a>
        </nav>
    </div>
    <div class="menu-list">
        <form action="LogOut" method="POST" class="form">
            <% String name=(String)request.getSession().getAttribute("name");%>
            <p><%=  name%> </p>
            <input type="submit" value="LOG OUT" class="logout"/>

        </form>
    </div>
</header>
<section>


    <table>
        <tr>
            <th>Fname</th>
            <th>Phone No.</th>
            <th>Email</th>
            <th>Message</th>
        </tr>
        <%

            List<ContactUs> list = (List<ContactUs>)request.getAttribute("contactUsList");
            for(ContactUs contactUs: list) {
        %>
        <tr>
            <td><%=contactUs.getName()%></td>
            <td><%=contactUs.getPhoneNo()%></td>
            <td><%=contactUs.getEmail()%></td>
            <td><%=contactUs.getQuery()%></td>
        </tr>
        <%
            }
        %>
    </table>
</section>
</body>
</html>