<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="withequaldevice-with, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="./css/index.css">
        <link href='https://fonts.googleapis.com/css?family=DM Serif Display' rel='stylesheet'>
    </head>
    <body>
        <%@include file="header.jsp"%>
        <div id="dropDownMenu">
            <div id="dropDownMenu-list">
                <nav>
                    <a id="dropDownMenu-a" href="#">HOW WE WORK</a><br>
                    <a id="dropDownMenu-a" href="#">BLOG</a><br>
                    <a id="dropDownMenu-a" href="#">ACCOUNT</a><br>
                    <a id="dropDownMenu-a" href="contactus.html">CONTACT US</a><br>
                    <a id="dropDownMenu-a" href="#">VIEW PLANS</a><br>
                </nav>
            </div>
            <div id="dropDownMenu-pattern">
                <img src="./images/bg-pattern-mobile-nav.svg">
            </div>
        </div>
        
        
        <div class="color">  
                <div class="layer1">
                    <img src="./images/bg-pattern-intro-right-desktop.svg" class="right">
                    <img src="./images/bg-pattern-intro-left-desktop.svg" class="left">
                    <hr>
                </div>      
                <div id="elementNav">
                    <img src="./images/bg-pattern-intro-left-mobile.svg" class="bg-pattern-intro-left-mobile">
                    <section>
                        <div>
                            <hr>
                        </div>
                        <h1 class="para-heading">    
                            Humanizing<br>
                            your insurance.
                        </h1>
                        <p>
                            Get your life insurance coverage easier and faster.We blend our expertise<br>
                            and technology to help you find the plan that's right for you.Ensure you <br>
                            and your loved ones are protected.
                        </p>
                        <a href="#viewInnerPlans" class="viewInnerButton">VIEW PLANS</a>
                    </section>
                    <aside>
                        <img src="./images/image-intro-desktop.jpg" class="centerImage" alt="family-image">
                        <img src="images/image-intro-mobile.jpg" class="centerImage1" alt="family-image">
                    </aside>
                    
                </div>
                <img src="./images/bg-pattern-intro-right-mobile.svg" class="bg-pattern-intro-right-mobile">
        </div>

         <div id="container_section">
                
            <h1 class="heading_different">We're different</h1>
            
            <div class="row">
                <section class="column">
                    <img src="./images/icon-snappy-process.svg">
                    <h1>Snappy Process</h1>
                    <p>Our application process can be completed in<br> minutes, not hours. Don't get stuck filling in <br>
                    ledious forms.</p>
                </section>
                <section class="column">
                    <img src="./images/icon-affordable-prices.svg">
                    <h1>Affordable Prices</h1>
                    <p>We don't want you worrying about high monthly <br>costs. Our prices may be low, but we still offer <br>
                    the best coverage possible.</p>
                    </section>
                <section class="column">
                    <img src="./images/icon-people-first.svg">
                    <h1>People First</h1>
                    <p>Our plans aren't full of conditions and clauses <br>to prevent payouts.We make sure you're <br>
                    covered when you need it.</p>
                </section>
            </div>
           
            <div class="howWeWork">
                <img src="./images/bg-pattern-how-we-work-desktop.svg" alt="left-head-image" id="leftHead" class="left-head-png">   
                    <img src="./images/bg-pattern-how-we-work-mobile.svg" alt="how-we-work-image" class="mobileHowWeWork">
                <div class="flex">
                    <h1 class="paraFindOutMore">Find out more <br>
                    about how we work</h1>
                <a href="#WeWork" class="addFeature">How we work</a>
                </div>
            </div>
        </div>
        <%@include file="footer.jsp"%>
    </body>
</html>
